#!/bin/bash
cd SUCHAI-Flight-Software
git pull
git checkout feature/framework
version=$(cmake -version)
echo $version
test=$1
echo $test
if [ "$test" = "test_unit" ]
then
    cmake -B build_test -DAPP=${test} -DTEST=1 -DSCH_OS=LINUX -DSCH_ARCH=X86 -DSCH_COMM_ENABLE=0 -DSCH_FP_ENABLED=0 -DSCH_ST_MODE=SQLITE -DSCH_STORAGE_TRIPLE_WR=1
    cmake --build build_test
    cd build_test/test/${test}
    ./suchai-test
    status=$?
    cd -
    echo "The exit status was $status"
    if [ "$status" -ne 0 ]
    then
        exit 1
    fi
fi
    

#!/bin/bash
cd suchai-2-software
git pull
git checkout framework
cd apps/plantsat/src/drivers/
sh install.sh
cd -
sh build_plantsat.sh
status=$?
echo "The exit status was $status"
if [ "$status" -ne 0 ]
then
    exit 1
fi
